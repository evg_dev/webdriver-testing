let btns = document.querySelectorAll('.Button');
console.log(btns);

if(btns !== null) {
  [...btns].forEach((btn)=> {
    btn.addEventListener('click', function(){
      let btnData = btn.getAttribute('data-btn');
      console.log(btnData);
    });
  });
}

window.onscroll = function(){
  let windowOffsetTop = window.pageYOffset;
  let SiteHeader = document.querySelector('.SiteHeader');
  if(windowOffsetTop > 20) {
    SiteHeader.classList.add('__fixed');
  } else {
    SiteHeader.classList.remove('__fixed');
  }
};

let Forms = document.querySelectorAll('.Form');

if(Forms !== null) {
  [...Forms].forEach((form)=> {
    form.addEventListener('submit', function(e){
      e.preventDefault();
      console.log('prevented');
    });
  });
}
