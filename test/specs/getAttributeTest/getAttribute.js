var webdriverio = require('webdriverio');
var expect = require('expect.js');
var assert = require('assert');
var server = 'http://10.0.2.15:9000';
var should = require('should');

beforeEach(function() {
  console.log(' ');
  console.log('getAttribute command test started');
});
afterEach(function() {
  console.log('getAttribute command test ended');
  console.log(' ');
});

describe('getAttribute command', function() {
  it('should demonstrate the getAttribute command', function () {
    browser.url(server);
    let formmEthod = browser.getAttribute('.Form', 'method'); // outputs: "post"
    // if your selector matches multiple elements it returns an array of results
    console.log(formmEthod + ' this is formmEthod');

  });
});
