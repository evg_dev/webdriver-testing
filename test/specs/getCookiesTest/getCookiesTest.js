var webdriverio = require('webdriverio');
var expect = require('expect.js');
var assert = require('assert');
var server = 'http://10.0.2.15:9000';
var should = require('should');

beforeEach(function() {
  console.log(' ');
  console.log('set and read cookie test started');
});
afterEach(function() {
  console.log('set and read cookie test ended');
  console.log(' ');
});

describe('set and read cookie', function() {
  it('should set a cookie for the page', function () {
      browser.url(server);
      browser.setCookie({name: 'test', value: '123'});
      var cookies = browser.getCookie();
      console.log(cookies);
  });
});
