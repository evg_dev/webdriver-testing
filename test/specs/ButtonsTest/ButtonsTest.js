var webdriverio = require('webdriverio');
var expect = require('expect.js');
var assert = require('assert');
var server = 'http://10.0.2.15:9000';
var should = require('should');

beforeEach(function() {
  console.log(' ');
  console.log('pushing buttons test started');
});
afterEach(function() {
  console.log('pushing buttons test ended');
  console.log(' ');
});

describe('pushing buttons', function() {
    it('- btns should be pushed', function () {
      browser.url(server);
      browser.click('.ButtonOne');
      console.log('clicked btn #1! 11');
      browser.click('.ButtonTwo');
      console.log('clicked btn #2!');
      browser.click('.ButtonThree');
      console.log('clicked btn #3!');
      browser.click('.ButtonFour');
      console.log('clicked btn #4!');
    });
});
