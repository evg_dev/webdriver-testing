var webdriverio = require('webdriverio');
var expect = require('expect.js');
var server = 'http://10.0.2.15:9000';

beforeEach(function() {
  console.log(' ');
  console.log('checking SiteHeader height test started');
});
afterEach(function() {
  console.log('checking SiteHeader height test ended');
  console.log(' ');
});

describe('checking SiteHeader height', function() {
  it('- SiteHeader height must be 62px', function () {
    browser.url(server);
    let header = $('.SiteHeader');
    let HeaderHeight = header.getElementSize('height');
    console.log(HeaderHeight);
    expect(HeaderHeight).to.be(62);
  });
});
