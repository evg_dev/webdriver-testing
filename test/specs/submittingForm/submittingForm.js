var webdriverio = require('webdriverio');
var expect = require('expect.js');
var server = 'http://10.0.2.15:9000';

beforeEach(function() {
  console.log(' ');
  console.log('set values to inputs test started');
});
afterEach(function() {
  console.log('set values to inputs test ended');
  console.log(' ');
});

var searchPage = {
  textInput: '.FormField',
  phoneInput: '.FormFieldNumber',
  msgArea: 'textarea'
}

describe('set values to inputs and submit form', function() {
    it('- should paste values to inputs and submit form', function () {
        browser.url(server);
        browser.pause(1000);
        browser.setValue(searchPage.textInput, 'test');
        browser.pause(1000);
        browser.setValue(searchPage.phoneInput, 89899879596);
        browser.pause(1000);
        browser.setValue(searchPage.msgArea,'test');
        browser.pause(1000);
        var textValue = browser.getValue(searchPage.textInput);
        expect(textValue).to.be.a('string');
        browser.submitForm('.Form');
        browser.pause(1000);
        browser.back();
        browser.pause(1000);
    });
});
